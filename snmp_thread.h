﻿#ifndef SNMP_THREAD_H
#define SNMP_THREAD_H

#include <QObject>
#include <QThread>
#include <QVector>
#include <QString>
#include <QTimer>
#include <QDebug>
#include <one_gui_datablock.h>
#include <snmppoll/snmppoll.hpp>

class Snmp_Thread : public QThread
{
    Q_OBJECT
public:
    explicit Snmp_Thread(QObject *parent = 0);
    ~Snmp_Thread();
    void run();

    void setThreadId(int num) {
        threadId = num;
    }
    void setGuiBlock(One_Gui_DataBlock* b) {
            block = b;
    }
    void setIpAddress(QString ip) {
        ipAddress = ip;
    }
    void setDataToGui(std::string fromSnmp []);

    void makeSnmpPoll();

private slots:
    void currentTabSlot(int index);
    void timerTimeoutSlot();

private:
    One_Gui_DataBlock* block;
    TMyApp* snmpApp;
    QString ipAddress;
    QTimer* timer;
    int threadId;
    
};

#endif // SNMP_THREAD_H
