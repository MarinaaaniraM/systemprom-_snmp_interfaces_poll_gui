﻿using namespace std;

#include <string>
#include <locale.h>
#include <stdlib.h>
#include "snmppoll/snmppoll.hpp"


#include <QDebug>


const int cmGetReq = 200;   // assign new command values
const int cmSetReq  = 201;
const int cmAbout  = 211;

/* SS: micro change here */
char host[128];
int lineCount = 0;
//int snmpGet(char *node, char *comm, char *param);

TMyApp::TMyApp()
{
    memset( host, 0, sizeof(host));
    WIN = new fstream("../snmp_interfaces_poll_gui/snmppoll/oids", ios::in);
    if( WIN ) {
        int n = 0;
        // заполним oid из файла
        while( WIN->is_open() && !WIN->eof() && n < MAX_POLL_PARAM) {
            string tmps;
            char tmpc[128];
            *WIN >> tmps;
            mib[n] = new Oid(tmps.c_str());
            WIN->getline(tmpc, 128);
            if(mib[n]){
                if(tmpc[0] == ' ')
                    mibDescr[n] += tmpc;
                n++;
            }
        }
        // обнулим неиспользованные oid
        for(; n < MAX_POLL_PARAM; mib[n++] = NULL);

        WIN->close();
        delete WIN;
    }
    // open log
    WIN = new fstream( "snmppoll.log", ios::out);
}

TMyApp::~TMyApp()
{
    if( WIN) {
        WIN->close();
        delete WIN;
    }
    for(int n = 0; n < MAX_POLL_PARAM; delete mib[n++]);
}


void TMyApp::setIpAddress(const char* ip) {
    if(inet_addr(ip) != INADDR_NONE)
        strncpy(host, ip, sizeof(host));
}


void TMyApp::runSelective(string snmpOutput[], int startOid, int countOid) {
    if(inet_addr(host) != INADDR_NONE || strlen(host) > 7) {

        int k = 0;
        for( int i = startOid; i < (startOid + countOid) && mib[i] != NULL; ++i)
        {
            string host_reply;
            char* currMib = NULL;
            currMib = mib[i]->get_printable();

            if(!snmpGet(host, "public", currMib, &host_reply, false, NULL)) {
                cout << "Error!" << endl;
                // Пробуем еще раз
                i --;
                k --;
                continue;
            }
            snmpOutput[k].clear();
            snmpOutput[k].append(host_reply);
            k ++;
        }
    }
}


void TMyApp::runIterative(string snmpOutput[], int startOid, int countOid,
                                               int startIterat, int countIterat) {

    if(inet_addr(host) != INADDR_NONE || strlen(host) > 7) {

        int k = 0;
        for( int i = startOid; i < (startOid + countOid) && mib[i] != NULL; ++i)
        {
            string host_reply;

            for (int j = startIterat; j < (startIterat + countIterat); ++j) {
                char intfNum[2];
                sprintf(intfNum, "%d", j);
                char* currMib = strcat(mib[i]->get_printable(), intfNum);

                host_reply.clear();
                if(!snmpGet(host, "public", currMib, &host_reply, false, NULL) ) {
                    cout << "Error" << endl;
                    // Пробуем еще раз
                    i --;
                    k --;
                    continue;
                }
                snmpOutput[k].clear();
                snmpOutput[k].append(host_reply);
                k ++;
            }
        }
    }
}


void TMyApp::runByPrevious(string snmpOutput[], int startOid, int count) {
    if(inet_addr(host) != INADDR_NONE || strlen(host) > 7) {

        int k = 0;
        for( int i = startOid; i <= startOid  && mib[i] != NULL; ++i)
        {
            string host_reply;
            char* currMib = NULL;
            currMib = mib[i]->get_printable();

            for (int j = 0; j < count; ++j) {
                if(!snmpGet(host, "public", currMib, &host_reply, true, &currMib)) {

                    cout << "Error!" << endl;
                    // Пробуем еще раз
                    j --;
                    k --;
                    continue;
                }
                snmpOutput[k].clear();
                snmpOutput[k].append(host_reply);
                k ++;
            }
        }
    }
}
