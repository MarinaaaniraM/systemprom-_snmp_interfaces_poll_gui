﻿#ifndef ONE_GUI_DATABLOCK_H
#define ONE_GUI_DATABLOCK_H

#include <QWidget>
#include <QString>
#include <QTextCodec>
#include <QLabel>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QGridLayout>

class One_Gui_DataBlock : public QWidget {
    Q_OBJECT
public:
    explicit One_Gui_DataBlock(QWidget *parent = 0);
    ~One_Gui_DataBlock();

    QGridLayout* getDataLayout();

    void setIPAddress(QString addr);
    void setInBytes(QString num);
    void setInUcastPkts(QString num);
    void setInNUcastPkts(QString num);
    void setOutBytes(QString num);
    void setOutUcastPkts(QString num);
    void setOutNUcastPkts(QString num);
    void setInErrors(QString num);
    void setOutErrors(QString num);


private:
    QLabel* labelIPAddress;
    QLabel* labelInBytes;
    QLabel* labelInUcastPackets;
    QLabel* labelInNUcastPackets;
    QLabel* labelOutBytes;
    QLabel* labelOutUcastPackets;
    QLabel* labelOutNUcastPackets;
    QLabel* labelInErrors;
    QLabel* labelOutErrors;

    QLineEdit* ipAddress;
    QLineEdit* inBytes;
    QLineEdit* inUcastPackets;
    QLineEdit* inNUcastPackets;
    QLineEdit* outBytes;
    QLineEdit* outUcastPackets;
    QLineEdit* outNUcastPackets;
    QLineEdit* inErrors;
    QLineEdit* outErrors;

    QGridLayout* dataLayout;
    
};

#endif // ONE_GUI_DATABLOCK_H
