﻿#include "one_gui_datablock.h"

One_Gui_DataBlock::One_Gui_DataBlock(QWidget *parent) :
    QWidget(parent) {
    QTextCodec* codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(codec);

    labelIPAddress = new QLabel(tr("IP адрес интерфейса:"), this);
    labelInBytes = new QLabel(tr("Принято байт:"), this);
    labelInUcastPackets = new QLabel(tr("Принято пакетов (ucast):"), this);
    labelInNUcastPackets = new QLabel(tr("Принято пакетов (nucast):"), this);
    labelInErrors = new QLabel(tr("Ошибки приема:"), this);
    labelOutBytes = new QLabel(tr("Передано байт:"), this);
    labelOutUcastPackets = new QLabel(tr("Передано пакетов (ucast):"), this);
    labelOutNUcastPackets = new QLabel(tr("Передано пакетов (nucast):"), this);
    labelOutErrors = new QLabel(tr("Ошибки передачи:"), this);

    ipAddress = new QLineEdit("-", this);
    inBytes = new QLineEdit(this);
    inUcastPackets = new QLineEdit(this);
    inNUcastPackets = new QLineEdit(this);
    outBytes = new QLineEdit(this);
    outUcastPackets = new QLineEdit(this);
    outNUcastPackets = new QLineEdit(this);
    inErrors = new QLineEdit(this);
    outErrors = new QLineEdit(this);

//    labelInterfaceName->setFixedSize(100, 20);

    dataLayout = NULL;
}


// ============================================================================
One_Gui_DataBlock::~One_Gui_DataBlock() {
    delete[] dataLayout;
}


// ============================================================================
void One_Gui_DataBlock::setIPAddress(QString addr) {
    ipAddress->setText(addr);
}

void One_Gui_DataBlock::setInBytes(QString num) {
    inBytes->setText(num);
}

void One_Gui_DataBlock::setInUcastPkts(QString num) {
    inUcastPackets->setText(num);
}

void One_Gui_DataBlock::setInNUcastPkts(QString num) {
    inNUcastPackets->setText(num);
}

void One_Gui_DataBlock::setOutBytes(QString num) {
    outBytes->setText(num);
}

void One_Gui_DataBlock::setOutUcastPkts(QString num) {
    outUcastPackets->setText(num);
}

void One_Gui_DataBlock::setOutNUcastPkts(QString num) {
    outNUcastPackets->setText(num);
}

void One_Gui_DataBlock::setInErrors(QString num) {
    inErrors->setText(num);
}

void One_Gui_DataBlock::setOutErrors(QString num) {
    outErrors->setText(num);
}


// ============================================================================
QGridLayout* One_Gui_DataBlock::getDataLayout() {
    dataLayout = new QGridLayout();

    ipAddress->setFrame(false);
    ipAddress->setReadOnly(true);
    dataLayout->addWidget(labelIPAddress, 1, 0);
    dataLayout->addWidget(ipAddress, 1, 1);

    inBytes->setFrame(false);
    inBytes->setReadOnly(true);
    dataLayout->addWidget(labelInBytes, 2, 0);
    dataLayout->addWidget(inBytes, 2, 1);

    inUcastPackets->setFrame(false);
    inUcastPackets->setReadOnly(true);
    dataLayout->addWidget(labelInUcastPackets, 3, 0);
    dataLayout->addWidget(inUcastPackets, 3, 1);

    inNUcastPackets->setFrame(false);
    inNUcastPackets->setReadOnly(true);
    dataLayout->addWidget(labelInNUcastPackets, 4, 0);
    dataLayout->addWidget(inNUcastPackets, 4, 1);

    outBytes->setFrame(false);
    outBytes->setReadOnly(true);
    dataLayout->addWidget(labelOutBytes, 5, 0);
    dataLayout->addWidget(outBytes, 5, 1);

    outUcastPackets->setFrame(false);
    outUcastPackets->setReadOnly(true);
    dataLayout->addWidget(labelOutUcastPackets, 6, 0);
    dataLayout->addWidget(outUcastPackets, 6, 1);

    outNUcastPackets->setFrame(false);
    outNUcastPackets->setReadOnly(true);
    dataLayout->addWidget(labelOutNUcastPackets, 7, 0);
    dataLayout->addWidget(outNUcastPackets, 7, 1);

    inErrors->setFrame(false);
    inErrors->setReadOnly(true);
    dataLayout->addWidget(labelInErrors, 8, 0);
    dataLayout->addWidget(inErrors, 8, 1);

    outErrors->setFrame(false);
    outErrors->setReadOnly(true);
    dataLayout->addWidget(labelOutErrors, 9, 0);
    dataLayout->addWidget(outErrors, 9, 1);

    return dataLayout;
}
