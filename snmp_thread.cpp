﻿#include "snmp_thread.h"

Snmp_Thread::Snmp_Thread(QObject *parent) :
    QThread(parent)
{
    block = NULL;
    timer = new QTimer(this);
    snmpApp = new TMyApp();
    connect(timer, SIGNAL(timeout()), this, SLOT(timerTimeoutSlot()));
}


Snmp_Thread::~Snmp_Thread() {
    delete snmpApp;
}


// ============================================================================
void Snmp_Thread::run() {
//    makeSnmpPoll();
    exec();
}


// ============================================================================
void Snmp_Thread::makeSnmpPoll() {
    std::string fromSnmp [16];
    snmpApp->setIpAddress(ipAddress.toStdString().c_str());
    snmpApp->runIterative(fromSnmp, 3, 8, threadId + 1, 1);
    setDataToGui(fromSnmp);
}


// ============================================================================
void Snmp_Thread::setDataToGui(std::string fromSnmp []) {
    if (block) {
        block->setInBytes(QString::fromStdString(fromSnmp[0]));
        block->setInUcastPkts(QString::fromStdString(fromSnmp[1]));
        block->setInNUcastPkts(QString::fromStdString(fromSnmp[2]));
        block->setOutBytes(QString::fromStdString(fromSnmp[3]));
        block->setOutUcastPkts(QString::fromStdString(fromSnmp[4]));
        block->setOutNUcastPkts(QString::fromStdString(fromSnmp[5]));
        block->setInErrors(QString::fromStdString(fromSnmp[6]));
        block->setOutErrors(QString::fromStdString(fromSnmp[7]));
    }
}


// ============================================================================
void Snmp_Thread::currentTabSlot(int index) {

    if (index == threadId) {
        makeSnmpPoll();
        timer->start(3000);
    }
    else
        timer->stop();
}


// ============================================================================
void Snmp_Thread::timerTimeoutSlot() {
    makeSnmpPoll();
}


