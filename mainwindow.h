﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGridLayout>
//#include <QVector>
//#include <QString>
#include <string>
#include <one_gui_datablock.h>
#include <snmp_thread.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void setIpAddress(char* ip);
    void createBlocks(string commonData[], string ipTable[], int ipSize);
    
private:
    Ui::MainWindow *ui;

    QVector<One_Gui_DataBlock*>* blocks;
    QVector<Snmp_Thread*>* threads;

    QString ipAddress;

    int prevIndex;

};

#endif // MAINWINDOW_H
