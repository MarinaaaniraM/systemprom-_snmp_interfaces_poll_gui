// ������ ������ ����������� ���������� SNMP ��� �������� ����������� /////////
// ��� "���������", ������ �����, 2011 (�) ////////////////////////////////////
// Email: zhavnis@mail.ru /////////////////////////////////////////////////////
using namespace std;

#include <string>
#include <locale.h>
#include "snmppoll.hpp"

const int cmGetReq = 200;   // assign new command values
const int cmSetReq  = 201;
const int cmAbout  = 211;

/* SS: micro change here */
char host[128];
int lineCount = 0;
//static short winNumber  = 0;    // initialize window number
//int snmpGet(char *node, char *comm, char *param);

TMyApp::TMyApp()
{
  memset( host, 0, sizeof(host));
  WIN = new fstream( "oids", ios::in);
  if( WIN ) {
    int n = 0;
    // �������� oid �� �����
    while( WIN->is_open() && !WIN->eof() && n < MAX_POLL_PARAM) {
      string tmps;
      char tmpc[128];
      *WIN>>tmps;
      mib[n] = new Oid(tmps.c_str());
//      *WIN>>tmpc;
//      if( tmpc==' ')
      WIN->getline(tmpc,128);
      if(mib[n]){
        if(tmpc[0]==' ')
         mibDescr[n] += tmpc;
        n++;
      }
    }
    // ������� ���������������� oid
    for(; n < MAX_POLL_PARAM; mib[n++] = NULL);

    WIN->close();
    delete WIN;
  }
// open log
	WIN = new fstream( "snmppoll.log", ios::out);
}

TMyApp::~TMyApp()
{
  if( WIN) {
    WIN->close();
    delete WIN;
  }

  for(int n=0; n < MAX_POLL_PARAM; delete mib[n++]);
}

void TMyApp::SetParam(int argc, char* argv[])
{
  int n;
  while( ( n = getopt( argc, argv, "h:" ) ) != -1 ) {
      switch( n ) {
        case 'h': if( inet_addr( optarg) == INADDR_NONE)
                    break;
                  strncpy( host, optarg, sizeof(host));
                  break;
        case '?': break;
      }
    }
}

// ����� ���� � ����� ����������� �� ������ ����-�������
void TMyApp::run( void)
{
//  for( int i=0; i < MAX_POLL_PARAM && mib[i]!=NULL; i++) {
//    Vb vb;                                  // construct a Vb object
//    vb.set_oid( *mib[i]);                       // set the Oid portion of the Vb
//    cout<<vb.get_printable_oid()<<endl;
//  }
// ��������� �������

// ���� do-while �� ������ ip �� ������������ ����� � ������ ��� �� SNMP  /////
// ������ ���� �������� �� 127.0.0.1 ��� �� ������ �� ��������� ������    ////
///////////////////////////////////////////////////////////////////////////////
  do {
//    int error_counter = 0; // ������� ������ snmp-������
    if( !strlen(host))     // ������ ��������� IP
      cin.getline( host, sizeof(host));
    if( inet_addr( host) == INADDR_NONE || strlen(host)<7)
      break;
    // ��������� �������
    fstream tpl("head.htm",ios::in);
    if( tpl.is_open()) {
      while( !tpl.eof()){
        string s;
        tpl>>s;
        cout<<s<<' ';
      }
      cout<<endl;
      tpl.close();
    }
    // ��������� ������
    cout<<"<CENTER>����: <B>"<<host<<"</B></CENTER>"<<endl;
    // ������� ����������� ������
    cout<<"<TABLE border=1 cellpadding=0 cellspacing=0>"<<endl;
    // ��������� �������
    cout<<"<TR><TH>��������</TH><TH>����� ����</TH></TR>"<<endl;
    for( int i=0; i < MAX_POLL_PARAM && mib[i]!=NULL/* && error_counter<2*/; ++i)
    {
      string host_reply;
      cout<<"<TR>";

      if( !snmpGet( host, "public", mib[i]->get_printable(), &host_reply) ) {
        cout<<"<TD COLSPAN=\"2\">������ ������ "<<host_reply<<"</TD></TR>"<<endl;
        break; // ����� ��� ������ ��������� ������ ��� ��������
      }

      if( mibDescr[i].length()>0)
        cout<<"<TD>"<<mibDescr[i]<<"</TD>";
      else
        cout<<"<TD>"<<mib[i]->get_printable()<<"</TD>";
      cout<<"<TD>"<<host_reply<<"</TD>"<<"</TR>"<<endl;
    }
    cout<<endl<<"</TABLE>"<<endl;
  // ������ �������
    tpl.open("foot.htm",ios::in);
    if( tpl.is_open()) {
      while( !tpl.eof()){
        string s;
        tpl>>s;
        cout<<s<<' ';
      }
      cout<<endl;
      tpl.close();
    }
    // ���������� ���������� IP
    host[0] = 0;
  } while(1);
}

int main( int argc, char* argv[])
{
  setlocale( LC_ALL, "ru_RU.koi8r");
  TMyApp myApp;
  myApp.SetParam( argc, argv);
  myApp.run();
  return 1;
}

