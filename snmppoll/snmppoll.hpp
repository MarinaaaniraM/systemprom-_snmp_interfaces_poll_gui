// ������ ������ ����������� ���������� SNMP ��� �������� ����������� /////////
// ��� "���������", ������ �����, 2011 (�) ////////////////////////////////////
// Email: zhavnis@mail.ru /////////////////////////////////////////////////////

#ifndef __SNMPPOLL_HPP_
#define __SNMPPOLL_HPP_

using namespace std;

#include <iostream>
#include <fstream>
#include <string>
#include "snmp_pp.h"

#include <QDebug>

#define MAX_POLL_PARAM 16

class TMyApp
{
public:
    TMyApp();
    virtual ~TMyApp();
    void setIpAddress(const char* ip);
    int snmpGet(char *node, char *comm, char *param, string* rep, int wantNext,
                                        char** returnOid);
    void runSelective(string snmpOutput[], int startOid, int countOid);
    void runIterative(string snmpOutput[], int startOid, int countOid,
                                           int startIterat, int countIterat);
    void runByPrevious(string snmpOutput[], int startOid, int count);
private:
    fstream* WIN;
    Oid* mib[MAX_POLL_PARAM];
    string mibDescr[MAX_POLL_PARAM];
    char host[16];
};

#endif
