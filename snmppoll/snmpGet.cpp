﻿/*
  snmpGet.cpp
*/
#include "snmppoll.hpp"

int TMyApp::snmpGet(char *node, char *comm, char *param, string* rep,
                                int wantNext, char** returnOid) {

   if( !rep){           // check validity of reply string
	  *WIN << "Invalid reply string supplied\n";
	  return 0;
   }
   //---------[ make a GenAddress and Oid object to retrieve ]---------------
   GenAddress address( node);      // make a SNMP++ Generic address
   if ( !address.valid()) {           // check validity of address
	  *WIN << "Invalid Address or DNS Name, " << node << "\n";
	  return 0;
   }


   Oid oid("1.3.6.1.2.1.1.1.0");      // defualt is sysDescr
   oid = param;
   if ( !oid.valid()) {         // check validity of user oid
    *WIN << "Invalid Oid, " << param << "\n";
    return 0;
   }

   //---------[ determine options to use ]-----------------------------------
   snmp_version version=version1;                       // default is v1
   int retries=1;                                       // default retries is 1
   int timeout=100;                                     // default is 1 second
   OctetStr community(comm);                            // community name

   //----------[ create a SNMP++ session ]-----------------------------------
   int status;
   Snmp snmp( status);                // check construction status
   if ( status != SNMP_CLASS_SUCCESS) {
      *WIN << "SNMP++ Session Create Fail, " << snmp.error_msg(status) << "\n";
      return 0;
   }

   //--------[ build up SNMP++ object needed ]-------------------------------
   Pdu pdu;                                // construct a Pdu object
   Vb vb;                                  // construct a Vb object
   vb.set_oid( oid);                       // set the Oid portion of the Vb
   pdu += vb;                              // add the vb to the Pdu
   CTarget target( address);               // make a target using the address
   target.set_version( version);           // set the SNMP version SNMPV1 or V2
   target.set_retry( retries);             // set the number of auto retries
   target.set_timeout( timeout);           // set timeout
   target.set_readcommunity( community);   // set read community
   target.set_writecommunity( community);  // set the write community name

   //-------[ issue the request, blocked mode ]-----------------------------
   *WIN << "SNMP++ Get to " << node << " SNMPV" << (version+1) << " Retries=" << retries;
   *WIN << " Timeout=" << timeout <<"ms " << "Community=" << community.get_printable() << "\n";

   if (( status = snmp.get( pdu,target))== SNMP_CLASS_SUCCESS) {
       pdu.get_vb(vb,0);
	   *WIN << "Oid = " << vb.get_printable_oid() << "\n";
	   *WIN << "Value = " << vb.get_printable_value() << "\n";

       if (wantNext) {
           if ((snmp.get_next(pdu,target)) == SNMP_CLASS_SUCCESS)
               pdu.get_vb(vb,0);

           strcpy(*returnOid, vb.get_printable_oid());
       }

       unsigned long int repPart = 0;
       if (vb.get_value(repPart) == 0) {    // If numbers
           char tempBuff[20];
           sprintf(tempBuff, "%u", repPart);
           *rep = tempBuff;
       } else                               // If text
           *rep =vb.get_printable_value();

   } else {
	   *WIN << "SNMP++ Get Error, ";
     if ( status == SNMP_CLASS_ERR_STATUS_SET)
	     status = pdu.get_error_status();
     *WIN << snmp.error_msg( status) << "\n";
     *rep+=snmp.error_msg( status);
     if( status == SNMP_CLASS_TIMEOUT) // return 0 only when timed out
       return 0;
   }

   return 1;
}  // end get

