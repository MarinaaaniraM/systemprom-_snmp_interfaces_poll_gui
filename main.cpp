﻿#include <QApplication>
#include <QVector>
#include <QDebug>
#include <string>
#include <mainwindow.h>
#include <snmppoll/snmppoll.hpp>

int main( int argc, char* argv[])
{
    char host[128];
    int n;
    while( (n = getopt(argc, argv, "h:")) != -1 ) {
        switch(n) {
        case 'h':
            if( inet_addr(optarg) == INADDR_NONE)
                break;
            strncpy(host, optarg, sizeof(host));
            break;
        }
    }

    setlocale( LC_ALL, "ru_RU.koi8r");
    string commonTable [12];
    string ipTable [20];
    TMyApp* app = new TMyApp();
    app->setIpAddress(host);

    app->runSelective(commonTable, 0, 2);
    int countOfInterfaces = atoi(commonTable[1].c_str());
    app->runIterative(commonTable + 2, 2, 1, 1, countOfInterfaces);
    app->runByPrevious(ipTable, 11, 2 * countOfInterfaces);

    int ipTableSize = 2 * countOfInterfaces;
    bool isEnding = false;
    for (int i = 0; i < 2 * countOfInterfaces; ++i) {
        if (ipTable[i].length() < 3)
            isEnding = true;
        if (ipTable[i].length() > 3 && isEnding)
            ipTableSize = i;
    }

    delete app;

    QApplication a(argc, argv);
    MainWindow mainWindow;
    mainWindow.setIpAddress(host);
    mainWindow.createBlocks(commonTable, ipTable, ipTableSize);
    mainWindow.show();

    return a.exec();
}






