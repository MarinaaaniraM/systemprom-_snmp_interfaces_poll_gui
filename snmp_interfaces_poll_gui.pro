QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = snmp_interfaces_poll_gui
TEMPLATE = app

LIBS += -Wl,-dn -L../snmp_interfaces_poll_gui/ -lsnmp++ -Wl,-dy

SOURCES += main.cpp\
        mainwindow.cpp \
    snmppoll/snmpGet.cpp \
    one_gui_datablock.cpp \
    snmp_poll.cpp \
    snmp_thread.cpp

HEADERS  += mainwindow.h\
    snmppoll/vb.h \
    snmppoll/usertimeout.h \
    snmppoll/userdefined.h \
    snmppoll/timetick.h \
    snmppoll/target.h \
    snmppoll/snmp_pp.h \
    snmppoll/snmpmsg.h \
    snmppoll/snmperrs.h \
    snmppoll/smival.h \
    snmppoll/smi.h \
    snmppoll/pdu_cont.h \
    snmppoll/pdu.h \
    snmppoll/oid_def.h \
    snmppoll/oid.h \
    snmppoll/octet.h \
    snmppoll/notifyqueue.h \
    snmppoll/msgqueue.h \
    snmppoll/msec.h \
    snmppoll/ipresolv.h \
    snmppoll/integer.h \
    snmppoll/gauge.h \
    snmppoll/ctr64.h \
    snmppoll/counter.h \
    snmppoll/collect.h \
    snmppoll/asn1.h \
    snmppoll/address.h \
    snmppoll/snmppoll.hpp \
    snmppoll/eventlist.h \
    one_gui_datablock.h \
    snmp_thread.h


FORMS    += mainwindow.ui\

