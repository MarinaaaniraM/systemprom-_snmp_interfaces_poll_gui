﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QPushButton>
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);

    prevIndex = -1;

    blocks = new QVector<One_Gui_DataBlock*>();
    threads = new QVector<Snmp_Thread*>();

    this->setFixedSize(500, 300);
}


// ============================================================================
MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::setIpAddress(char *ip) {
    ipAddress = QString::fromLocal8Bit(ip);
}


// ============================================================================
void MainWindow::createBlocks(string commonData[], string ipTable[], int ipSize) {
    this->setWindowTitle(QString::fromStdString(commonData[0]));

    for (int i = 0; i < atoi(commonData[1].c_str()); ++i) {

        One_Gui_DataBlock* block = new One_Gui_DataBlock();
        Snmp_Thread* thread = new Snmp_Thread();
        QWidget* widget = new QWidget(ui->tabWidget);
        QGridLayout* grid = new QGridLayout(ui->tabWidget);

        for (int j = ipSize / 2; j < ipSize; ++j) {
            if (atoi(ipTable[j].c_str()) == i + 1)
                block->setIPAddress(QString::fromStdString(
                                        ipTable[j - ipSize / 2]));
        }

        grid->addLayout(block->getDataLayout(), 0, i + 1);
        widget->setLayout(grid);

        ui->ipAddr->setText(ipAddress);
        ui->tabWidget->addTab(widget, QString::fromStdString(commonData[2 + i]));

        blocks->append(block);
        threads->append(thread);

        thread->setGuiBlock(block);
        thread->setIpAddress(ipAddress);
        thread->setThreadId(i);
        thread->start();

        connect(ui->tabWidget, SIGNAL(currentChanged(int)),
                thread, SLOT(currentTabSlot(int)));
    }
    ui->tabWidget->setCurrentIndex(1);
    ui->tabWidget->setCurrentIndex(0);
}




















